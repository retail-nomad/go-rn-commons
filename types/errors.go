package types

import "errors"

var ErrLabel = "error"

// General Errors
var (
	ErrInvalidRequest = errors.New("invalid request")
	ErrUnauthorized   = errors.New("unauthorized access")
	ErrForbidden      = errors.New("forbidden access")
	ErrNotFound       = errors.New("resource not found")
	ErrInternalServer = errors.New("internal server error")
)

// Authentication Errors
var (
	ErrInvalidCredentials = errors.New("invalid credentials")
	ErrTokenExpired       = errors.New("token has expired")
	ErrTokenInvalid       = errors.New("invalid token")
	ErrTokenNotFound      = errors.New("token not found")
)

// Validation Errors
var (
	ErrValidationFailed = errors.New("validation failed")
	ErrEmailInvalid     = errors.New("invalid email format")
	ErrPasswordWeak     = errors.New("password is too weak")
)

// Database Errors
var (
	ErrDBConnection  = errors.New("failed to connect to the database")
	ErrDBQueryFailed = errors.New("database query failed")
)

// Service-Specific Errors (SSO)
var (
	ErrUserAlreadyExists = errors.New("user already exists")
	ErrUserNotFound      = errors.New("user not found")
	ErrSessionExpired    = errors.New("session expired")
	ErrSessionInvalid    = errors.New("invalid session")
)
