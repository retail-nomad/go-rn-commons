package main

import (
	"fmt"
	"gitlab.com/retail-nomad/go-rn-commons/config"
	"log"
)

func main() {
	dbConfig, err := config.LoadDBConfig()
	if err != nil {
		log.Fatalf("Error loading database config: %v", err)
	}

	fmt.Printf("%+v\n", dbConfig)
}
