.air.tomlgen:
	@protoc --go_out=. --go_opt=paths=source_relative \
      --go-grpc_out=. --go-grpc_opt=paths=source_relative \
      api/oms.proto

build:
	@go build -o bin/test_project cmd/main.go

test:
	@go test -v ./...

run: build
	@./bin/test_project
