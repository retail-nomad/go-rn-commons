package config

type DBConfig struct {
	DBHost     string `mapstructure:"DB_HOST"`
	DBName     string `mapstructure:"DB_NAME"`
	DBUser     string `mapstructure:"DB_USER"`
	DBPort     string `mapstructure:"DB_PORT"`
	DBPassword string `mapstructure:"DB_PASSWORD"`
	Recover    bool   `mapstructure:"RECOVER"`
	Tracing    bool   `mapstructure:"TRACING"`
	Prefork    bool   `mapstructure:"PREFORK"`
}

type DBTestConfig struct {
	DBHost     string `mapstructure:"TEST_DB_HOST"`
	DBName     string `mapstructure:"TEST_DB_NAME"`
	DBUser     string `mapstructure:"TEST_DB_USER"`
	DBPort     string `mapstructure:"TEST_DB_PORT"`
	DBPassword string `mapstructure:"TEST_DB_PASSWORD"`
	Recover    bool   `mapstructure:"TEST_RECOVER"`
	Tracing    bool   `mapstructure:"TEST_TRACING"`
	Prefork    bool   `mapstructure:"TEST_PREFORK"`
}

var envs = []string{
	"DB_HOST",
	"DB_NAME",
	"DB_USER",
	"DB_PORT",
	"DB_PASSWORD",
	"RECOVER",
	"TRACING",
	"PREFORK",
}

var testEnvs = []string{
	"TEST_DB_HOST",
	"TEST_DB_NAME",
	"TEST_DB_USER",
	"TEST_DB_PORT",
	"TEST_DB_PASSWORD",
	"TEST_RECOVER",
	"TEST_TRACING",
	"TEST_PREFORK",
}

var defaultConfig = DBConfig{
	DBHost:     "localhost",
	DBName:     "",
	DBUser:     "postgres",
	DBPort:     "5432",
	DBPassword: "",
	Recover:    true,
	Tracing:    false,
	Prefork:    true,
}

func LoadDBConfig() (DBConfig, error) {
	var dbConfig DBConfig

	err := LoadConfig(&dbConfig, envs)
	if err != nil {
		return defaultConfig, err
	}

	return dbConfig, nil
}

func LoadTestDBConfig() (DBTestConfig, error) {
	var dbConfig DBTestConfig

	err := LoadConfig(&dbConfig, testEnvs)
	if err != nil {
		return DBTestConfig{}, err
	}

	return dbConfig, nil
}
