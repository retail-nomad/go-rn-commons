package config

import (
	"syscall"

	"github.com/joho/godotenv"
)

var EnvFilePath string

// SetEnvPath sets the environment file path
func SetEnvPath(path string) {
	EnvFilePath = path
}

// InitEnv initializes the environment variables
func InitEnv() error {
	if EnvFilePath != "" {
		return godotenv.Load(EnvFilePath)
	} else {
		return godotenv.Load()
	}
}

// EnvString loads the environment variable from the specified file path or default location.
func EnvString(key string, fallback ...string) string {
	if err := InitEnv(); err != nil {
		if len(fallback) > 0 {
			return fallback[0]
		}
		return ""
	}

	if val, ok := syscall.Getenv(key); ok {
		return val
	}

	if len(fallback) > 0 {
		return fallback[0]
	}

	return ""
}
