package config

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"

	"github.com/go-playground/validator/v10"
)

// LoadConfig loads configuration into the provided structure from environment variables
func LoadConfig(config interface{}, envs []string) error {
	err := InitEnv()
	if err != nil {
		return err
	}

	v := reflect.ValueOf(config).Elem()
	for _, key := range envs {
		field := findFieldByMapStructureTag(v, key)
		if !field.IsValid() {
			return errors.New("invalid field: " + key)
		}

		val := EnvString(key)
		if val == "" {
			return errors.New("environment variable " + key + " is required")
		}

		err := setFieldValue(field, val)
		if err != nil {
			return err
		}
	}

	if err := validator.New().Struct(config); err != nil {
		return err
	}

	return nil
}

// Helper function to find struct field by mapstructure tag
func findFieldByMapStructureTag(v reflect.Value, tag string) reflect.Value {
	for i := 0; i < v.NumField(); i++ {
		field := v.Type().Field(i)
		if field.Tag.Get("mapstructure") == tag {
			return v.Field(i)
		}
	}
	return reflect.Value{}
}

// Helper function to set field value correctly based on its type
func setFieldValue(field reflect.Value, val string) error {
	switch field.Kind() {
	case reflect.String:
		field.SetString(val)
	case reflect.Bool:
		boolVal, err := strconv.ParseBool(val)
		if err != nil {
			return fmt.Errorf("invalid boolean value for field %s: %s", field.Type().Name(), val)
		}
		field.SetBool(boolVal)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		intVal, err := strconv.ParseInt(val, 10, 64)
		if err != nil {
			return fmt.Errorf("invalid integer value for field %s: %s", field.Type().Name(), val)
		}
		field.SetInt(intVal)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		uintVal, err := strconv.ParseUint(val, 10, 64)
		if err != nil {
			return fmt.Errorf("invalid unsigned integer value for field %s: %s", field.Type().Name(), val)
		}
		field.SetUint(uintVal)
	case reflect.Float32, reflect.Float64:
		floatVal, err := strconv.ParseFloat(val, 64)
		if err != nil {
			return fmt.Errorf("invalid float value for field %s: %s", field.Type().Name(), val)
		}
		field.SetFloat(floatVal)
	default:
		return fmt.Errorf("unsupported field type: %s", field.Type().Name())
	}
	return nil
}
