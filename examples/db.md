# Пример использования базы данных

Этот пример показывает, как загрузить конфигурацию базы данных и использовать ее для подключения к базе данных
PostgreSQL.

## Пример использования config.go и db.go

```go
package main

import (
	"fmt"
	"gitlab.com/retail-nomad/go-rn-commons/config"
	"gitlab.com/retail-nomad/go-rn-commons/database"
	"log"

	fiberConfig "github.com/thnkrn/go-fiber-clean-arch/pkg/config"
)

func main() {
	config.SetEnvPath(".env")
	dbConfig, err := config.LoadDBConfig()
	if err != nil {
		log.Fatalf("Error loading database config: %v", err)
	}

	db, err := database.ConnectDatabase(fiberConfig.Config(dbConfig))
	if err != nil {
		log.Fatalf("Error connecting to database: %v", err)
	}

	database.DB = db

	defer database.CloseDatabase(database.DB)

	fmt.Printf("Successfully connected to database: %s\n", dbConfig.DBName)
}
```

## Пример использования setup.go и database.go

Этот пример показывает, как использовать пакет для подключения к базе данных PostgreSQL и выполнения миграций.

```go
package main

import (
	"fmt"
	"github.com/thnkrn/go-fiber-clean-arch/pkg/domain"
	"gitlab.com/retail-nomad/go-rn-commons/config"
	"gitlab.com/retail-nomad/go-rn-commons/database"
	"log"

	fiberConfig "github.com/thnkrn/go-fiber-clean-arch/pkg/config"
)

func main() {
	config.SetEnvPath(".env")
	dbConfig, err := config.LoadDBConfig()
	if err != nil {
		log.Fatalf("Error loading database config: %v", err)
	}

	db, err := database.ConnectDatabase(fiberConfig.Config(dbConfig))
	if err != nil {
		log.Fatalf("Error connecting to database: %v", err)
	}
	defer database.CloseDatabase(db)

	models := []interface{}{
		&domain.User{},
		// Добавьте здесь другие модели
	}

	err = database.MigrateDatabase(db, models)
	if err != nil {
		log.Fatalf("Error migrating database: %v", err)
	}

	fmt.Printf("Successfully migrated database: %s\n", dbConfig.DBName)
}
```