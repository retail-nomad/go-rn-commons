# Пример использования env.go

Этот пример показывает, как настроить и использовать инициализацию переменных окружения в Go.

## Пример использования в main.go

```go
package main

import (
	"fmt"
	"gitlab.com/retail-nomad/go-rn-commons/config"
	"log"
)

func main() {
	config.SetEnvPath(".env")
	err := config.InitEnv()
	if err != nil {
		log.Fatalf("Error initializing environment: %v", err)
	}

	dbHost := config.EnvString("DB_HOST", "localhost")
	dbUser := config.EnvString("DB_USER", "postgres")

	fmt.Printf("Database Host: %s\n", dbHost)
	fmt.Printf("Database User: %s\n", dbUser)
}
```