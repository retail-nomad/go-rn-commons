package database

import (
	"fmt"

	"github.com/thnkrn/go-fiber-clean-arch/pkg/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// ConnectDatabase connects to the database
func ConnectDatabase(cfg config.Config) (*gorm.DB, error) {
	psqlInfo := fmt.Sprintf("host=%s user=%s dbname=%s port=%s password=%s", cfg.DBHost, cfg.DBUser, cfg.DBName, cfg.DBPort, cfg.DBPassword)
	db, dbErr := gorm.Open(postgres.Open(psqlInfo), &gorm.Config{
		SkipDefaultTransaction: true,
	})

	if dbErr != nil {
		return nil, dbErr
	}

	return db, nil
}

// CloseDatabase closes the database connection
func CloseDatabase(db *gorm.DB) {
	sqlDB, _ := db.DB()
	err := sqlDB.Close()
	if err != nil {
		return
	}
}

// MigrateDatabase performs the database migrations
func MigrateDatabase(db *gorm.DB, models []interface{}) error {
	for _, model := range models {
		err := db.AutoMigrate(model)
		if err != nil {
			return err
		}
	}
	return nil
}
